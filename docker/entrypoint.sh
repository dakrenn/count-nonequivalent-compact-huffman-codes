#!/bin/bash
USER_ID=${USER_ID:-1000}

echo "Starting with UID : $USER_ID"

if ! grep -q ":$USER_ID:" /etc/passwd; then
    useradd --shell /bin/bash -u $USER_ID -o -c "" -m user
fi

chown -R $USER_ID /src/output
exec /usr/local/bin/gosu user "$@"
