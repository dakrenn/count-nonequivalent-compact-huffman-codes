#include "fmpz_poly.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void calculate_coefficients(fmpz_poly_t result, unsigned long t, unsigned long N) {
    unsigned long j, hp_j, sigma_j, i;
    signed long sign;

    fmpz_poly_t coefficient_product;
    fmpz_poly_t coefficient_product_shifted;
    fmpz_poly_t new_factor;
    fmpz_poly_t numerator;
    fmpz_poly_t denominator;
    fmpz_poly_t aux;
    /* aux holds various intermediate results without reallocating memory in every step when using aliasing between input and output */

    clock_t start, end, start_division;
    double cpu_time_used, cpu_time_used_numden, cpu_time_used_division;

    start = clock();

    fmpz_poly_init2(coefficient_product, N);
    fmpz_poly_init2(coefficient_product_shifted, 2*N);
    fmpz_poly_init2(new_factor, N);
    fmpz_poly_init2(numerator, N);
    fmpz_poly_init2(denominator, N);
    fmpz_poly_init2(aux, N);

    fmpz_poly_set_ui(numerator, 1);
    fmpz_poly_set_ui(denominator, 1);
    fmpz_poly_set_ui(coefficient_product, 1);

    j=0;
    hp_j=0;
    sigma_j=0;
    sign=1;

    while(-1) {
        j++;
        hp_j=1+t*hp_j;
        sigma_j+=hp_j;
        sign=-sign;
        /* hp_j=[j]; sigma_j=sum_{i=1}^j [j]; sign=(-1)^j */

        if(sigma_j>=N) {
            break;
        }

        for(i=hp_j; i<N; i+=hp_j) {
            fmpz_poly_set_coeff_ui(new_factor, i, 1);
        }
        /* new_factor = q^[j]/(1-q^[j]) */

        fmpz_poly_mullow(aux, coefficient_product, new_factor, N);
        fmpz_poly_swap(coefficient_product, aux);
        /* coefficient_product=prod_{i=1}^j q^[i]/(1-q^[i]) */

        for(i=hp_j; i<N; i+=hp_j) {
            fmpz_poly_set_coeff_ui(new_factor, i, 0);
        }
        /* new_factor = 0 */

        if(sign==1) {
            fmpz_poly_add(denominator, denominator, coefficient_product);

            if(hp_j+sigma_j<N) {
                fmpz_poly_set(coefficient_product_shifted, coefficient_product);
                fmpz_poly_truncate(coefficient_product_shifted, N-hp_j);
                fmpz_poly_shift_left(coefficient_product_shifted, coefficient_product_shifted, hp_j);
                fmpz_poly_add(numerator, numerator, coefficient_product_shifted);
            }
        } else {
            fmpz_poly_sub(denominator, denominator, coefficient_product);

            if(hp_j+sigma_j<N) {
                fmpz_poly_set(coefficient_product_shifted, coefficient_product);
                fmpz_poly_truncate(coefficient_product_shifted, N-hp_j);
                fmpz_poly_shift_left(coefficient_product_shifted, coefficient_product_shifted, hp_j);
                fmpz_poly_sub(numerator, numerator, coefficient_product_shifted);
            }
        }
        /* numerator up to summand j; denominator up to summand j */
    }

    fmpz_poly_clear(coefficient_product);
    fmpz_poly_clear(coefficient_product_shifted);
    fmpz_poly_clear(new_factor);
    fmpz_poly_clear(aux);

    end = clock();
    cpu_time_used_numden = ((double) (end - start)) / CLOCKS_PER_SEC;

    start_division=clock();

    fmpz_poly_div_series(result, numerator, denominator, N);

    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    cpu_time_used_division = ((double) (end - start_division)) / CLOCKS_PER_SEC;
    printf("t=%lu, N=%lu, numerator&denominator: %.3f, division: %.3f, total: %.3f \n",
	   t, N, cpu_time_used_numden, cpu_time_used_division, cpu_time_used);

    fmpz_poly_clear(numerator);
    fmpz_poly_clear(denominator);

}

void main(int argc, char* argv[]) {

    fmpz_poly_t result;
    long n;
    long t;
    long N;
    fmpz_t coefficient;
    char filename[256];
    FILE *fp;

    if(argc<3) {
        printf("Usage: number_unit_fractions t N\n");
        exit(-1);
    }

    t=atol(argv[1]);
    N=atol(argv[2]);

    fmpz_poly_init(result);
    calculate_coefficients(result, t, N);

    sprintf(filename, "output/result_t=%lu_N=%lu.txt", t, N);
    fp=fopen(filename, "w");
    fmpz_init(coefficient);
    for(n=0; n<N; n++) {
        fmpz_poly_get_coeff_fmpz(coefficient, result, n);
        fmpz_fprint(fp, coefficient);
        fprintf(fp, "\n");
    }
    fclose(fp);
    fmpz_clear(coefficient);
    fmpz_poly_clear(result);
    exit(0);
}
