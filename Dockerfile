FROM buildpack-deps:stretch

RUN apt-get update -y  \
    && apt-get install -y \
    libflint-dev \
    && rm -rf /var/lib/apt/lists/*

COPY docker/B42F6819007F00F88E364FD4036A9C25BF357DD4.key /tmp/
RUN gpg --import /tmp/B42F6819007F00F88E364FD4036A9C25BF357DD4.key \
    && curl -o /usr/local/bin/gosu -SL "https://github.com/tianon/gosu/releases/download/1.10/gosu-$(dpkg --print-architecture)" \
    && curl -o /usr/local/bin/gosu.asc -SL "https://github.com/tianon/gosu/releases/download/1.10/gosu-$(dpkg --print-architecture).asc" \
    && gpg --verify /usr/local/bin/gosu.asc \
    && rm /usr/local/bin/gosu.asc \
    && chmod +x /usr/local/bin/gosu

COPY docker/entrypoint.sh /usr/local/bin/entrypoint.sh

WORKDIR /src
COPY . /src
ENV PATH=/src:/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin

RUN make

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
